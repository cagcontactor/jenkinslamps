/*
 * Created by KDMA02 2013-09-30 15:29
 */
package se.caglabs.jenkinslamps;

import org.joda.time.LocalTime;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BuildIndicatorTest {
    @Test
    public void testTwoJobsWhenAllJobsOk() throws Exception {
        runTest(
                true, null,
                Arrays.asList("unit-test", "integration-test"),
                new TreeMap<String, Map<String, JobStatus>>() {{
                    put("http://url1", new TreeMap<String, JobStatus>() {{
                        put("unit-test", JobStatus.ok);
                        put("integration-test", JobStatus.ok);
                    }});
                }},
                Arrays.asList(new Action(true, EventType.whenAllJobsOk))
        );
    }

    @Test
    public void testTwoJobsOkWhenAnyJobFails() throws Exception {
        runTest(
                false, null,
                Arrays.asList("unit-test", "integration-test"),
                new TreeMap<String, Map<String, JobStatus>>() {{
                    put("http://url1", new TreeMap<String, JobStatus>() {{
                        put("unit-test", JobStatus.ok);
                        put("integration-test", JobStatus.ok);
                    }});
                }},
                Arrays.asList(new Action(true, EventType.whenAnyJobFails))
        );
    }

    @Test
    public void testTwoJobsOkFailedWhenAnyJobFails() throws Exception {
        runTest(
                true, null,
                Arrays.asList("unit-test", "integration-test"),
                new TreeMap<String, Map<String, JobStatus>>() {{
                    put("http://url1", new TreeMap<String, JobStatus>() {{
                        put("unit-test", JobStatus.ok);
                        put("integration-test", JobStatus.failed);
                    }});
                }},
                Arrays.asList(new Action(true, EventType.whenAnyJobFails))
        );
    }

    @Test
    public void testTwoJobsOkWhenAnyJobUndefined() throws Exception {
        runTest(
                false, null,
                Arrays.asList("unit-test", "integration-test"),
                new TreeMap<String, Map<String, JobStatus>>() {{
                    put("http://url1", new TreeMap<String, JobStatus>() {{
                        put("unit-test", JobStatus.ok);
                        put("integration-test", JobStatus.ok);
                    }});
                }},
                Arrays.asList(new Action(true, EventType.whenAnyJobUndefined))
        );
    }

    @Test
    public void testTwoJobsUnknownOkWhenAnyJobUndefined() throws Exception {
        runTest(
                true, null,
                Arrays.asList("unit-test", "integration-test"),
                new TreeMap<String, Map<String, JobStatus>>() {{
                    put("http://url1", new TreeMap<String, JobStatus>() {{
                        put("unit-test", JobStatus.unknown);
                        put("integration-test", JobStatus.ok);
                    }});
                }},
                Arrays.asList(new Action(true, EventType.whenAnyJobUndefined))
        );
    }

    @Test
    public void testTwoJobWithJenkinsUrlOnLamp() throws Exception {
        runTest(
                true, "http://url1",
                Arrays.asList("unit-test", "integration-test"),
                new TreeMap<String, Map<String, JobStatus>>() {{
                    put("http://url1", new TreeMap<String, JobStatus>() {{
                        put("unit-test", JobStatus.ok);
                        put("integration-test", JobStatus.ok);
                    }});
                }},
                Arrays.asList(new Action(true, EventType.whenAllJobsOk))
        );
    }

    private void runTest(boolean lampState,
                         String lampJenkinsUrl,
                         Collection<String> jobNames, final Map<String, Map<String, JobStatus>> jobStatusMap,
                         List<Action> actions) throws Exception {
        LampConfig config = new LampConfig(
                new JenkinsConfig("http://url1", null),
                0,
                LocalTime.now().minusMinutes(1),
                LocalTime.now().plusMinutes(1),
                true,
                Arrays.asList(new Lamp(
                        "green",
                        "desc",
                        "cmd-on",
                        "cmd-off",
                        lampJenkinsUrl != null ? new JenkinsConfig(lampJenkinsUrl, null) : null,
                        jobNames,
                        actions)));
        JobStatusFetcherFactory factory = new JobStatusFetcherFactory() {
            @Override
            public JobStatusFetcher create(final JenkinsConfig jenkinsConfig) {
                return new JobStatusFetcher() {
                    @Override
                    public Map<String, JobStatus> getJobStatus(Collection<String> jobNames) {
                        return jobStatusMap.get(jenkinsConfig.getUrl());
                    }
                };
            }
        };
        CommandControllerStub controller0 = new CommandControllerStub(config.getLamps().get(0).getName());
        BuildIndicator b = new BuildIndicator(factory, config, Arrays.asList((LampController) controller0));
        assertThat(controller0.getState(), is(false));
        b.check();
        assertThat(controller0.getState(), is(lampState));
    }
}
