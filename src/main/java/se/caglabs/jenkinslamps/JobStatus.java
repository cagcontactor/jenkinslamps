/*
 * Created by KDMA02 2013-09-30 14:58
 */
package se.caglabs.jenkinslamps;

public enum JobStatus {
    ok, failed, error, unknown
}
