/*
 * Created by Daniel Marell 14-11-12 07:45
 */
package se.caglabs.jenkinslamps;

public interface JobStatusFetcherFactory {
    JobStatusFetcher create(JenkinsConfig jenkinsConfig);
}
