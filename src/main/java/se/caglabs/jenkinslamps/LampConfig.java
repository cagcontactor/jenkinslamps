/*
 * Created by KDMA02 2013-09-24 17:12
 */
package se.caglabs.jenkinslamps;

import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

enum EventType {whenAllJobsOk, whenAnyJobFails, whenAnyJobUndefined}

class Action {
    private boolean on;
    private EventType event;

    Action(boolean on, EventType event) {
        this.on = on;
        this.event = event;
    }

    boolean isOn() {
        return on;
    }

    EventType getEvent() {
        return event;
    }
}

class Lamp {
    private String name;
    private String description;
    private String onCommand;
    private String offCommand;
    private JenkinsConfig jenkinsConfig;
    private List<String> jobNames = new ArrayList<String>();
    private List<Action> actions = new ArrayList<Action>();

    Lamp(String name, String description, String onCommand, String offCommand, JenkinsConfig jenkinsConfig,
         Collection<String> jobNames, List<Action> actions) {
        this.name = name;
        this.description = description;
        this.onCommand = onCommand;
        this.offCommand = offCommand;
        this.jenkinsConfig = jenkinsConfig;
        this.jobNames = new ArrayList<String>(jobNames);
        this.actions = actions;
    }

    String getName() {
        return name;
    }

    String getDescription() {
        return description;
    }

    String getOnCommand() {
        return onCommand;
    }

    String getOffCommand() {
        return offCommand;
    }

    public JenkinsConfig getJenkinsConfig() {
        return jenkinsConfig;
    }

    List<String> getJobNames() {
        return jobNames;
    }

    List<Action> getActions() {
        return actions;
    }
}

class JenkinsConfig {
    private String url;
    private Credentials credentials;

    public JenkinsConfig(String url, Credentials credentials) {
        this.url = url;
        this.credentials = credentials;
    }

    public String getUrl() {
        return url;
    }

    public Credentials getCredentials() {
        return credentials;
    }
}

class Credentials {
    private String userName;
    private String password;

    public Credentials(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}

public class LampConfig {
    private JenkinsConfig jenkinsConfig;
    private int pollTimeMsec;
    private LocalTime turnOnTime;
    private LocalTime turnOffTime;
    private boolean activeHolidays;
    private List<Lamp> lamps = new ArrayList<Lamp>();

    public LampConfig(JenkinsConfig jenkinsConfig, int pollTimeMsec, LocalTime turnOnTime, LocalTime turnOffTime,
                      boolean activeHolidays, List<Lamp> lamps) {
        this.jenkinsConfig = jenkinsConfig;
        this.pollTimeMsec = pollTimeMsec;
        this.turnOnTime = turnOnTime;
        this.turnOffTime = turnOffTime;
        this.activeHolidays = activeHolidays;
        this.lamps = lamps;
    }

    public JenkinsConfig getJenkinsConfig() {
        return jenkinsConfig;
    }

    public int getPollTimeMsec() {
        return pollTimeMsec;
    }

    public LocalTime getTurnOnTime() {
        return turnOnTime;
    }

    public LocalTime getTurnOffTime() {
        return turnOffTime;
    }

    public boolean isActiveHolidays() {
        return activeHolidays;
    }

    public List<Lamp> getLamps() {
        return lamps;
    }
}
