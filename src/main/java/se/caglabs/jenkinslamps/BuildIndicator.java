/*
 * Created by Daniel Marell 12-12-03 11:17 PM
 */
package se.caglabs.jenkinslamps;

import org.apache.log4j.Logger;
import org.joda.time.LocalTime;

import java.util.*;

/**
 * Controls build indicator lamps:
 * Red=one or more builds has failed
 * Green=All build are ok
 * <p/>
 * The class reads its configuration from a Configuration object.
 */
public class BuildIndicator {
    private static final Logger logger = Logger.getLogger(BuildIndicator.class);
    private LampConfig config;
    private SwedishHolidayExplorer holiday = new SwedishHolidayExplorer();
    private List<LampController> lampControllers;
    private Map<String, JobStatusFetcher> jobStatusFetcherMap = new HashMap<String, JobStatusFetcher>();
    private JobStatusFetcherFactory factory;

    public BuildIndicator(JobStatusFetcherFactory factory, LampConfig config, List<LampController> lampControllers) {
        this.factory = factory;
        this.config = config;
        this.lampControllers = lampControllers;
        logger.info("Started CAG jenkinslamps, jenkinsUrl=" + config.getJenkinsConfig().getUrl());
    }

    public void check() {
        for (Lamp lamp : config.getLamps()) {
            Map<String, JobStatus> jobStatusMap = getJobStatusFetcherMap(getJenkinsConfig(lamp, config)).getJobStatus(
                    findAllJobsNames(config));
            LampController controller = findLampController(lampControllers, lamp);
            assert controller != null : "Expected it to be impossible to not get a LampController here";
            LocalTime now = LocalTime.now();
            String s = holiday.getHoliday(Calendar.getInstance());
            if (s != null || now.isBefore(config.getTurnOnTime()) || now.isAfter(config.getTurnOffTime())) {
                // Holiday or outside office time
                controller.turnLamp(false);
            } else {
                for (Action action : lamp.getActions()) {
                    switch (action.getEvent()) {
                        case whenAllJobsOk:
                            if (allJobsOk(lamp.getJobNames(), jobStatusMap)) {
                                controller.turnLamp(action.isOn());
                            }
                            break;
                        case whenAnyJobFails:
                            if (anyJobFails(lamp.getJobNames(), jobStatusMap)) {
                                controller.turnLamp(action.isOn());
                            }
                            break;
                        case whenAnyJobUndefined:
                            if (anyJobUndefined(lamp.getJobNames(), jobStatusMap)) {
                                controller.turnLamp(action.isOn());
                            }
                            break;
                        default:
                            throw new IllegalStateException("Unknown EventType: " + action.getEvent());
                    }
                }
            }
        }
    }

    private JobStatusFetcher getJobStatusFetcherMap(JenkinsConfig jenkinsConfig) {
        JobStatusFetcher result = jobStatusFetcherMap.get(jenkinsConfig);
        if (result == null) {
            result = factory.create(jenkinsConfig);
        }
        return result;
    }

    private JenkinsConfig getJenkinsConfig(Lamp lamp, LampConfig config) {
        if (lamp.getJenkinsConfig() != null) {
            return lamp.getJenkinsConfig();
        }
        return config.getJenkinsConfig();
    }

    private boolean allJobsOk(List<String> jobNames, Map<String, JobStatus> jobStatusMap) {
        for (String jobName : jobNames) {
            JobStatus s = jobStatusMap.get(jobName);
            assert s != null;
            if (s != JobStatus.ok) {
                return false;
            }
        }
        return true;
    }

    private boolean anyJobFails(List<String> jobNames, Map<String, JobStatus> jobStatusMap) {
        for (String jobName : jobNames) {
            JobStatus s = jobStatusMap.get(jobName);
            assert s != null;
            if (s == JobStatus.error || s == JobStatus.failed) {
                return true;
            }
        }
        return false;
    }

    private boolean anyJobUndefined(List<String> jobNames, Map<String, JobStatus> jobStatusMap) {
        for (String jobName : jobNames) {
            JobStatus s = jobStatusMap.get(jobName);
            assert s != null;
            if (s == JobStatus.unknown) {
                return true;
            }
        }
        return false;
    }

    private Set<String> findAllJobsNames(LampConfig config) {
        Set<String> result = new HashSet<String>();
        for (Lamp lamp : config.getLamps()) {
            result.addAll(lamp.getJobNames());
        }
        return result;
    }

    private LampController findLampController(List<LampController> controllers, Lamp lamp) {
        for (LampController c : controllers) {
            if (c.getName().equals(lamp.getName())) {
                return c;
            }
        }
        return null;
    }
}
