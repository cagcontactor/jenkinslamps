/*
 * Created by KDMA02 2013-09-30 15:33
 */
package se.caglabs.jenkinslamps;

import org.apache.http.*;
import org.apache.http.auth.*;
import org.apache.http.client.*;
import org.apache.http.client.methods.*;
import org.apache.http.client.protocol.*;
import org.apache.http.impl.auth.*;
import org.apache.http.impl.client.*;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.net.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JenkinsJobStatusFetcher implements JobStatusFetcher {
    private static final Logger logger = Logger.getLogger(JenkinsJobStatusFetcher.class);
//    private URL url;
    private HttpHost host;
    private HttpClientContext context = HttpClientContext.create();
    private CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
    private HttpGet httpGet;

    public JenkinsJobStatusFetcher(JenkinsConfig jenkinsConfig) throws MalformedURLException {
        try {
            URL url = new URL(jenkinsConfig.getUrl());
            this.host =  new HttpHost(url.getHost(), url.getPort(), url.getProtocol());

            if (jenkinsConfig.getCredentials() != null) {
                credentialsProvider.setCredentials(
                        new AuthScope(url.getHost(), url.getPort()),
                        new UsernamePasswordCredentials(jenkinsConfig.getCredentials().getUserName(), jenkinsConfig.getCredentials().getPassword()));

                // Create AuthCache instance
                AuthCache authCache = new BasicAuthCache();

                // Generate BASIC scheme object and add it to the local auth cache
                BasicScheme basicAuth = new BasicScheme();
                authCache.put(host, basicAuth);

                // Add AuthCache to the execution context
                context.setCredentialsProvider(credentialsProvider);
                context.setAuthCache(authCache);
            }

            httpGet = new HttpGet(url.toString());
        } catch (MalformedURLException e) {
            logger.error("Failed to parse api url (" + jenkinsConfig.getUrl() + "):" + e.getMessage());
            throw e;
        }
    }

    @Override
    public Map<String, JobStatus> getJobStatus(Collection<String> jobNames) {
        CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultCredentialsProvider(credentialsProvider)
                .build();
        Map<String, JobStatus> result = new HashMap<String, JobStatus>();
        CloseableHttpResponse response = null;
        Document dom;

        try {
            response = httpclient.execute(host, httpGet, context);
            dom = new SAXReader().read(response.getEntity().getContent());
        } catch (DocumentException e) {
            logger.error("Failed to parse api url (" + host.toString() + "):" + e.getMessage());
            for (String s : jobNames) {
                result.put(s, JobStatus.unknown);
            }
            return result;
        } catch (ClientProtocolException e) {
            logger.error("Error for URL (" + host.toString() + "):" + e.getMessage());
            for (String s : jobNames) {
                result.put(s, JobStatus.unknown);
            }
            return result;
        } catch (IOException e) {
            logger.error("Error for URL (" + host.toString() + "):" + e.getMessage());
            for (String s : jobNames) {
                result.put(s, JobStatus.unknown);
            }
            return result;
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    logger.error("Failed to close connection: " + e.getMessage(), e);
                }
            }
        }

        for (Element jobElement : (List<Element>) dom.getRootElement().elements("job")) {
            String jobName = jobElement.elementText("name");
            if (jobNames.contains(jobName)) {
                String ballColor = jobElement.elementText("color");
                logger.debug("jobName=" + jobName + ",ballColor=" + ballColor);
                JobStatus jobStatus;
                if (ballColor.startsWith("blue") || ballColor.startsWith("green")) {
                    jobStatus = JobStatus.ok;
                } else if (ballColor.startsWith("yellow")) {
                    jobStatus = JobStatus.failed;
                } else if (ballColor.startsWith("red")) {
                    jobStatus = JobStatus.error;
                } else {
                    jobStatus = JobStatus.unknown;
                }
                result.put(jobName, jobStatus);
            }
        }
        return result;
    }
}
