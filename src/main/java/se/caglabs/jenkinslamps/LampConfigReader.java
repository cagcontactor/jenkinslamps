/*
 * Created by KDMA02 2013-09-30 13:07
 */
package se.caglabs.jenkinslamps;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.joda.time.LocalTime;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class LampConfigReader {
    /**
     * Read a LampConfig object from an XML file
     *
     * @param configIs Configuration file
     * @return The MyData object
     */
    public static LampConfig read(InputStream configIs) throws IOException {
        SAXBuilder builder = new SAXBuilder();
        try {
            Document doc = builder.build(configIs);
            Element root = doc.getRootElement();

            JenkinsConfig jenkinsConfig = parseJenkinsConfig(root);

            List<Lamp> lamps = parseLamps(root);

            return new LampConfig(
                    jenkinsConfig,
                    Integer.parseInt(root.getChild("pollTimeMsec").getText()),
                    LocalTime.parse(root.getChild("turnOn").getText()),
                    LocalTime.parse(root.getChild("turnOff").getText()),
                    root.getChild("activeHolidays").getText().equals("true"),
                    lamps);
        } catch (JDOMException e) {
            throw new IOException("Malformed XML file", e);
        }
    }

    private static List<Lamp> parseLamps(Element lampsElement) throws IOException {
        Element lampCommandElement = lampsElement.getChild("lamps");
        List<Element> list = lampCommandElement.getChildren();
        List<Lamp> result = new ArrayList<Lamp>();
        for (Element cmdElement : list) {
            JenkinsConfig jenkinsConfig = parseJenkinsConfig(cmdElement);

            result.add(new Lamp(
                    cmdElement.getChild("name").getText(),
                    cmdElement.getChild("description").getText(),
                    cmdElement.getChild("onCommand").getText(),
                    cmdElement.getChild("offCommand").getText(),
                    jenkinsConfig,
                    parseJobNames(cmdElement.getChild("jobs")),
                    parseActions(cmdElement.getChild("actions"))
            ));
        }
        return result;
    }

    private static JenkinsConfig parseJenkinsConfig(Element root) {
        JenkinsConfig jenkinsConfig = null;
        Element jenkinsConfigElement = root.getChild("jenkinsConfig");
        Element jenkinsUrlElement = root.getChild("jenkinsUrl");
        if (jenkinsConfigElement != null) {
            String url = null;
            Credentials credentials = null;
            Element e = jenkinsConfigElement.getChild("url");
            if (e != null) {
                url = e.getText();
            }

            Element credentialsElement = jenkinsConfigElement.getChild("credentials");
            if (credentialsElement != null) {
                Element userName = credentialsElement.getChild("userName");
                Element password = credentialsElement.getChild("password");
                credentials = new Credentials(userName != null ? userName.getText() : null,
                        password != null ? password.getText() : null);
            }
            jenkinsConfig = new JenkinsConfig(url, credentials);
        } else if (jenkinsUrlElement != null) {
            jenkinsConfig = new JenkinsConfig(jenkinsUrlElement.getText(), null);
        }

        return jenkinsConfig;
    }

    private static List<Action> parseActions(Element actionsElement) {
        List<Action> result = new ArrayList<Action>();
        List<Element> list = actionsElement.getChildren();
        for (Element e : list) {
            result.add(new Action(
                    e.getText().equals("on"),
                    EventType.valueOf(e.getName())
            )
            );
        }
        return result;
    }

    private static List<String> parseJobNames(Element jobsElement) {
        List<String> result = new ArrayList<String>();
        List<Element> list = jobsElement.getChildren();
        for (Element jobElement : list) {
            result.add(jobElement.getText());
        }
        return result;
    }
}
