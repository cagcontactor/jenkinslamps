/*
 * Created by Daniel Marell 2013-01-31 17:06
 */
package se.caglabs.jenkinslamps;

public interface LampController {
    String getName();

    void turnLamp(boolean on);
}
