/*
 * Created by Daniel Marell 12-12-03 10:22 PM
 */
package se.caglabs.jenkinslamps;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Java client to set static of lamps depending on Jenkins build status.
 * <p/>
 * Command line parameters: <configFileName> Path to configuration file. If omitted, the file "config.xml"
 * is assumed in cwd.
 */
public class Main {
    public static void main(String[] args) {
        String filename;
        if (args.length > 0) {
            filename = args[0];
        } else {
            filename = "config.xml";
        }
        try {
            LampConfig config = LampConfigReader.read(new FileInputStream(new File(filename)));

            List<LampController> lampControllers = new ArrayList<LampController>();
            for (Lamp lamp : config.getLamps()) {
                lampControllers.add(new ExternalCommandController(lamp.getName(), lamp.getOnCommand(), lamp.getOffCommand()));
            }
            BuildIndicator controller = new BuildIndicator(
                    new JobStatusFetcherFactory() {
                        @Override
                        public JobStatusFetcher create(JenkinsConfig jenkinsConfig) {
                            try {
                                return new JenkinsJobStatusFetcher(jenkinsConfig);
                            } catch (MalformedURLException e) {
                                System.out.println("Failed to parse url: " + jenkinsConfig.getUrl());
                                System.exit(-117);
                                return null;
                            }
                        }
                    },
                    config,
                    lampControllers);
            while (true) {
                controller.check();
                try {
                    Thread.sleep(config.getPollTimeMsec());
                } catch (InterruptedException ignore) {
                }
            }
        } catch (IOException e) {
            System.out.println("Failed to read config file " + filename + ":" + e.getMessage());
        }
    }
}